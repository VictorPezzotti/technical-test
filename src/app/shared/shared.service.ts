import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class SharedService {
    public baseUrl: string;

    constructor(
        private http: HttpClient,
        ) {
        this.baseUrl = environment.apiUrl;
    }

    getSuperHeroes() {
        return this.http.get(this.baseUrl)
    }
}
