import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component';
import { SuperheroCardComponent } from './superhero-card/superhero-card.component';

const appRoutes: Routes = [
    {
      path: 'home', component: HomeComponent, pathMatch: 'full'
    },
    {
      path: 'superhero-card', component: SuperheroCardComponent, pathMatch: 'full'
    },
    {
      path: '',
      redirectTo: 'home', pathMatch: 'full'
    },
    {
      path: '**',
      redirectTo: 'home', pathMatch: 'full'
    }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        preloadingStrategy: PreloadAllModules,
          useHash: true }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}