import { Component } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-superhero-card',
  templateUrl: './superhero-card.component.html',
  styleUrls: ['./superhero-card.component.css']
})
export class SuperheroCardComponent {
  p: number = 1;
  superHeroes: any;
  sharedService: SharedService;
  private cookieValue:string;
  likeStatus: boolean;
  dislikeStatus: boolean;
  page:boolean = false;
  modalOption: NgbModalOptions = {};
  modalLinks: any;
  heroToShow: {
    name:string,
    picture:any,
    info:string,
    publisher:string
  };

constructor( private _sharedService: SharedService, 
    private router: Router, private modalService: NgbModal ) { 
    this.sharedService = this._sharedService;
    this._sharedService.getSuperHeroes().subscribe(
      response => {
       this.superHeroes = response;
       })
  }

openHeroModal(heroModal, i: number){
  console.log("heroes", this.superHeroes);
  this.heroToShow = this.superHeroes[i];
  this.modalOption.ariaLabelledBy = 'modal-basic-title';
  this.modalOption.size = 'lg';
  if (this.heroToShow) {this.modalLinks = this.modalService.open(heroModal, this.modalOption);}
}

setCoockieValue(button, i){
   if (button == 'like') {
     if (this.likeStatus) {
       this.likeStatus = false;
      } else{ 
       this.likeStatus = true;
       this.dislikeStatus = false;
      }
     }
   else if(button == "dislike"){
    if (this.dislikeStatus) { 
    this.dislikeStatus = false;
    }else {
    this.dislikeStatus = true;
    this.likeStatus = false;
  }
  }
 }
}
